/**
 * Created by Piotr on 14.04.2016.
 */
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

public class TPSIServer {
    public static String rootDir = "";
    public static String path = "";


    public static void main(String[] args) throws Exception {
        int port = 8000;
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/", new RootHandler());
        //server.createContext("/get", new getHandler());
        System.out.println("Starting server on port: " + port);
        System.out.println(args[0]);
        rootDir = args[0];
        server.start();
    }

    static class RootHandler implements HttpHandler {
        public void handle(HttpExchange exchange) throws IOException {
            File dir;
            byte[] response = new byte[0];
            String hostPath = exchange.getRequestURI().toString();
            hostPath = rootDir + hostPath.replace("/","\\");
            if (FilenameUtils.normalize(hostPath).startsWith(rootDir)) {
                System.out.println(hostPath);
                dir = new File(hostPath);
                if (dir.isFile()) {
                    Path path = Paths.get(dir.toURI());
                    response = Files.readAllBytes(path);
                    exchange.getResponseHeaders().set("Content-Type", "text/plain");
                    exchange.sendResponseHeaders(200, response.length);
                } else {
                    File[] files = dir.listFiles();
                    if (files != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("<!DOCTYPE html><html><head><title>" +
                                "Page Title</title><meta charset=\"UTF-8\"></head><body><ul>");
                        for (int i = 0; i < files.length; i++) {
                            String name;
                            name = files[i].getName();
                            sb.append("<li><a href=\"/").append(files[i].getPath().replaceFirst(Pattern.quote(rootDir+"\\"),"")).append("\">").append(name).append("</a></li>");
                        }
                        sb.append("</ul></body></html>");

                        response = sb.toString().getBytes();
                        exchange.getResponseHeaders().set("Content-Type", "text/html");
                        exchange.sendResponseHeaders(200, response.length);
                    }else{
                        String resp = "<!DOCTYPE html><html><head><title>" +
                                "Page Title</title><meta charset=\"UTF-8\"></head><body><h1>404 Not Found</h1></body></html>";
                        response = resp.getBytes();
                        exchange.sendResponseHeaders(404, response.length);
                    }
                }
            } else {
                String resp = "<!DOCTYPE html><html><head><title>" +
                        "Page Title</title><meta charset=\"UTF-8\"></head><body><h1>403 Forbidden</h1></body></html>";
                response = resp.getBytes();
                exchange.sendResponseHeaders(403, response.length);
            }

            OutputStream os = exchange.getResponseBody();
            os.write(response);
            os.close();
        }
    }
}