/**
 * Created by Piotr on 18.04.2016.
 */
import java.io.File;
import java.io.IOException;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class BlackListFilter {
    private ArrayList<Pattern> filters = new ArrayList<>();

    public BlackListFilter(){
        File file = new File("filter.txt");
        List<String> rules;
        try{
            rules = Files.readAllLines(file.toPath());
            rules.stream().filter(rule -> !rule.isEmpty()).forEach(this::addFilter);
        }catch(IOException ex)
        {
            ex.printStackTrace();
        }
    }
    public void addFilter(String regex){
        filters.add(Pattern.compile(regex));
    }

    public boolean canConnect(URI uri){
        for (Pattern filter:filters) {
            Matcher matcher = filter.matcher(uri.toString());
            if(matcher.find())
                return false;
        }
        return true;
    }
}
