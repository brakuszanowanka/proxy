import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Piotr on 18.04.2016.
 */
public class Logger
{
    Map<String, Integer> uris = new HashMap<>();
    String file = "log.csv";
    private String getDomain(URI uri){
        String domain = uri.getHost();
        return domain.startsWith("www.") ? domain.substring(4) : domain;
    }

    public void log(URI uri){
        String domain = getDomain(uri);
        uris.put(domain, uris.getOrDefault(domain, 0) + 1);

    }

    public void dump(){
        String eol = System.getProperty("line.separator");

        try (Writer writer = new FileWriter(file)) {
            for (Map.Entry<String, Integer> entry : uris.entrySet()) {
                writer.append(entry.getKey())
                        .append(';')
                        .append(entry.getValue().toString())
                        .append(eol);
            }
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
    }
}
