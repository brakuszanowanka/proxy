import com.sun.glass.ui.SystemClipboard;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.net.*;

import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;

/**
 * Created by Piotr on 08.04.2016.
 */
public class HttpProxy {
    public static void main (String[] args) throws Exception {
        int port = 8000;
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/", new RootHandler());
        System.out.println("Starting server on port: " + port);
        server.start();
    }

    private static void rewriteExchangeHeaders(Map<String, List<String>> headers, HttpURLConnection connection){
        for (Map.Entry<String, List<String>> entry : headers.entrySet()) {
            String key = entry.getKey();
            for(String value: entry.getValue())
            {
                connection.addRequestProperty(key, value);
            }
        }
    }

    private static void rewriteHeaders(Map<String, List<String>> headers, HttpExchange exchange){
        for (Map.Entry<String, List<String>> entry : headers.entrySet()) {
            String key = entry.getKey();
            if(key!=null && !key.equals("Transfer-Encoding"))
            for(String value: entry.getValue())
            {
                exchange.getResponseHeaders().add(key, value);
            }
        }
    }

    private static void rewriteExchangeBody(HttpExchange incoming, HttpURLConnection outgoing) throws IOException {
        rewriteExchangeBody(incoming.getRequestBody(), outgoing);
    }

    private static void rewriteExchangeBody(InputStream inputStream, HttpURLConnection outgoing) throws IOException{
        byte[] requestBodyData;
        requestBodyData = IOUtils.toByteArray(inputStream);
        if(requestBodyData.length<=0)
        {
            return;
        }
        outgoing.setDoOutput(true);
        OutputStream os = outgoing.getOutputStream();
        os.write(requestBodyData);
        os.close();
    }

    private static void rewriteBodyAndSend(int responseCode, HttpURLConnection outbound, HttpExchange inbound) throws IOException{
        try{
            byte[] response = IOUtils.toByteArray(outbound.getInputStream());
            inbound.sendResponseHeaders(responseCode, response.length);
            OutputStream os = inbound.getResponseBody();
            if(response.length > 0)
                os.write(response);
            os.close();
            inbound.close();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }



    private static HttpURLConnection forwardInboundToOutbound(HttpExchange incoming) throws IOException{
        HttpURLConnection outgoing = null;
        try{
            String method = incoming.getRequestMethod();
            outgoing = (HttpURLConnection)incoming.getRequestURI().toURL().openConnection();
            outgoing.setRequestMethod(method);
            rewriteExchangeHeaders(incoming.getRequestHeaders(), outgoing);
            if(method.equals("POST") || method.equals("PUT"))
                rewriteExchangeBody(incoming, outgoing);
            outgoing.connect();
        }catch(Exception ex){
            ex.printStackTrace();
        }

        return outgoing;
    }

    private static void forwardOutboundToInbound(HttpURLConnection outbound, HttpExchange inbound) throws IOException{
        rewriteHeaders(outbound.getHeaderFields(), inbound);
        int responseCode = outbound.getResponseCode();
        rewriteBodyAndSend(responseCode, outbound, inbound);
    }

    private static class RootHandler implements HttpHandler {
        Logger logger = new Logger();
        private static BlackListFilter blackListFilter = new BlackListFilter();

        public void handle(HttpExchange exchange) throws IOException {
            System.out.println(exchange.getRequestURI().toString());

            logger.log(exchange.getRequestURI());

            if(blackListFilter.canConnect(exchange.getRequestURI())) {
                HttpURLConnection remoteConnection = forwardInboundToOutbound(exchange);
                forwardOutboundToInbound(remoteConnection, exchange);
            }
            logger.dump();
        }
    }

}
